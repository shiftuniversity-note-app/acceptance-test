/* globals gauge*/
const { $, click, goto, write, textBox, button, listItem } = require('taiko');

//When I land on http://localhost:8080 I want to see the most simple components on the page so will know where to go
step('Given: that <link> is active', async (link) => {
	await goto(link);
});

step('Then: I should see the textbox id: <id>', async (id) => {
	const inputExist = await $(`#${id}`).exists();
	if (!inputExist) throw `I should see textbox`;
});

step('Then: I should see my note list. id: <i>', async (i) => {
	const listExist = await $(`#${i}`).exists();
	if (!listExist) throw `I should see my list`;
});

//When I type something in the “textbox” and click save button Then i should see my note in the list.

step('When I type <text> id: <id>', async (text, id) => {
	await write(text, textBox({ id: id }));
});
step('Then click <id> button', async (id) => {
	await click(button({ id: id }));
});

step('Then: I should see my <text> in the list', async (text) => {
	const res = await listItem(text).exists();
	if (!res) throw `I should see test item in ${text}`;
});
