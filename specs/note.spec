# Note App accaptance tests

## When I land on http://localhost:8080 I want to see the most simple components on the page so will know where to go

* Given: that "http://localhost:8080" is active
* Then: I should see the textbox id: "textbox"
* Then: I should see my note list. id: "list"

## When I type something in the “textbox” and click save button Then i should see my note in the list.

* When I type "lorem" id: "textbox"
* Then click "save" button
* Then: I should see my "lorem" in the list
